import { Transport } from '@nestjs/microservices';
import { readdirSync } from 'fs';
import { join } from 'path';
import { AUTHENTICATION_PACKAGE_NAME } from './interfaces/proto/authentication';

const protoPath = '../node_modules/@shoppe/shoppe-proto/proto';
const protoFiles = readdirSync(join(__dirname, protoPath));
export const grpcClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: [AUTHENTICATION_PACKAGE_NAME],
    protoPath: protoFiles.map((protoFiles) =>
      join(__dirname, `${protoPath}/${protoFiles}`),
    ),
  },
};
