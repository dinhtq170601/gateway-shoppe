import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthenticationController } from 'src/controller/admin/authentication.controller';
import { ADMIN_AUTHENTICATION_SERVICE_NAME } from 'src/interfaces/proto/authentication';
import { GRPCFactory } from 'src/utils/grpc-factory';

@Module({
  imports: [ConfigModule],
  controllers: [AuthenticationController],
  providers: [
    {
      provide: ADMIN_AUTHENTICATION_SERVICE_NAME,
      useFactory: GRPCFactory,
      inject: [ConfigService],
    },
  ],
})
export class AuthenticationModule {}
