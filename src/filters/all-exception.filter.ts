import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';

function isJsonString(str: string): boolean {
  try {
    JSON.parse(str);
  } catch (error) {
    return false;
  }
  return true;
}

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    if (exception instanceof HttpException) {
      return response
        .status(exception.getResponse()['statusCode'])
        .json(exception.getResponse());
    }

    const parsedException = JSON.parse(
      JSON.stringify(exception, (key, value) => {
        return key === 'details' &&
          typeof value === 'string' &&
          isJsonString(value)
          ? JSON.parse(value)
          : value;
      }),
    );

    if (
      parsedException.details &&
      parsedException.details.status &&
      parsedException.details.message
    ) {
      return response.status(parsedException.details.status).json({
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Lỗi server',
      });
    }
  }
}
