import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import cookieParser from 'cookie-parser';
import { json, urlencoded } from 'express';
import 'reflect-metadata';
import 'source-map-support';
import chalk from 'chalk';
import { AppModule } from './app.module';
import { IoAdapter } from '@nestjs/platform-socket.io';
import { AllExceptionsFilter } from './filters/all-exception.filter';
import { TransformResponseIntercepter } from './interceptors/transfrm-response.interceptors';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule, {
    bodyParser: true,
  });

  const allowedOrigins = ['*'];
  const configService = app.get(ConfigService);
  app.useWebSocketAdapter(new IoAdapter(app));
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  app.enableCors({
    origin: (origin, callback) => {
      if (allowedOrigins[0].startsWith('*')) {
        return callback(null, true);
      }
      if (!origin) return callback(null, true);
      if (allowedOrigins.indexOf(origin) === -1) {
        const msg =
          'The CORS policy for this site does not allow access from the specified Origin.';
        console.info(msg, origin);
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 200,
    credentials: true,
    allowedHeaders: 'Content-Type, Accept, Authorization',
    exposedHeaders: ['Content-Disposition'],
  });
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );
  app.setGlobalPrefix('api');
  app.use(cookieParser());
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalFilters(new AllExceptionsFilter());
  app.useGlobalInterceptors(new TransformResponseIntercepter());
  app.listen(configService.get('PORT') || 4000).then(() => {
    console.info(
      chalk.bold.blue(`App listen on port ${configService.get('PORT')}`),
    );

    console.info(
      chalk.bold.blue(
        `grpc-shoppe-manager service is listening on port ${configService.get(
          'GRPC_CONNECTION_URL',
        )}`,
      ),
    );
    console.info(
      chalk.bold.blue(
        `grpc-shoppe-user gRPC service is listening on port ${configService.get(
          'USER_GRPC_CONNECTION_URL',
        )}`,
      ),
    );
  });
}
bootstrap();
