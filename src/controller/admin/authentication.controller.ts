import { Metadata } from '@grpc/grpc-js';
import {
  Body,
  Controller,
  HttpCode,
  Inject,
  OnModuleInit,
  Post,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { LoginDto } from 'src/dtos/Login.dto';
import { ICustomResponse } from 'src/interfaces/custom-response.interface';
import {
  ADMIN_AUTHENTICATION_SERVICE_NAME,
  AdminAndTokens,
  AdminAuthenticationServiceClient,
} from 'src/interfaces/proto/authentication';

@Controller('admin-authentication')
export class AuthenticationController implements OnModuleInit {
  private authenticationServiceClient: AdminAuthenticationServiceClient;
  constructor(
    @Inject(ADMIN_AUTHENTICATION_SERVICE_NAME)
    private readonly clientGRPC: ClientGrpc,
  ) {}

  async onModuleInit(): Promise<void> {
    this.authenticationServiceClient =
      this.clientGRPC.getService<AdminAuthenticationServiceClient>(
        ADMIN_AUTHENTICATION_SERVICE_NAME,
      );
  }

  @Post('log-in')
  @HttpCode(200)
  async login(@Body() dto: LoginDto): Promise<ICustomResponse<AdminAndTokens>> {
    const adminAndTokens = await lastValueFrom(
      this.authenticationServiceClient.logIn(dto, new Metadata()),
    );
    delete adminAndTokens.admin.password;
    delete adminAndTokens.admin.hashedRefreshToken;

    return {
      result: adminAndTokens,
      message: 'Đăng nhập thành công',
    };
  }
}
