import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { join } from 'path';
import Joi from 'joi';
import { AuthenticationModule } from './module/admin.module';

const isProduction = process.env.NODE_ENV === 'production';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'uploads'),
      serveRoot: '/uploads',
    }),
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        PORT: Joi.number().required(),

        GRPC_CONNECTION_URL: Joi.string().required(),
        USER_GRPC_CONNECTION_URL: Joi.string().required(),

        FRONTEND_URL: Joi.string().required(),

        JWT_ACCESS_TOKEN_SECRET: Joi.string().required(),
        JWT_ACCESS_TOKEN_EXPIRATION_TIME: Joi.string().required(),
        JWT_REFRESH_TOKEN_SECRET: Joi.string().required(),
        JWT_REFRESH_TOKEN_EXPIRATION_TIME: Joi.string().required(),

        TWILIO_ACCOUNT_SID: Joi.string().required(),
        TWILIO_AUTH_TOKEN: Joi.string().required(),
        TWILIO_VERIFICATION_SERVICE_SID: Joi.string().required(),

        SSL_ROOT_CERT_PATH: isProduction
          ? Joi.string().required()
          : Joi.string().optional(),

        USER_ACCESS_COOKIE_NAME: Joi.string().required(),
        USER_REFRESH_COOKIE_NAME: Joi.string().required(),

        ACCESS_COOKIE_NAME: Joi.string().required(),
        REFRESH_COOKIE_NAME: Joi.string().required(),

        WEBSOCKET_ORIGIN: Joi.string().required(),
        REDIS_PORT: Joi.string().required(),
      }),
    }),
    AuthenticationModule,
  ],
  controllers: [],
  providers: [ConfigService],
})
export class AppModule {}
