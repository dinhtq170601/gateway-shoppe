import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, lastValueFrom } from 'rxjs';
import { instanceToPlain } from 'class-transformer';
import { map } from 'rxjs/operators';

export class Response<T> {
  statusCode: number;
  message: string;
  data: T;
}

@Injectable()
export class TransformResponseIntercepter<T>
  implements NestInterceptor<T, Promise<Response<T>>>
{
  intercept(
    context: ExecutionContext,
    call$: CallHandler,
  ): Observable<Promise<Response<T>>> {
    return call$.handle().pipe(
      map(async (data) => {
        if (!data) {
          return context.switchToHttp().getResponse();
        }

        if (data.result instanceof Observable) {
          return {
            statusCode: context.switchToHttp().getResponse().statusCode,
            message: data.message,
            data: instanceToPlain(
              await lastValueFrom(data.result),
            ) as unknown as T,
          };
        }
        return {
          statusCode: context.switchToHttp().getResponse().statusCode,
          message: data.message,
          data: instanceToPlain(data.result) as unknown as T,
        };
      }),
    );
  }
}
