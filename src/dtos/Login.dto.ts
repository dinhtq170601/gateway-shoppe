import { IsString } from 'class-validator';
import { PhoneAndPassword } from 'src/interfaces/proto/authentication';

export class LoginDto implements PhoneAndPassword {
  @IsString({ message: 'Yêu cầu nhập số điện thoại' })
  phoneNumber;

  @IsString({ message: 'Yêu cầu nhập mật khẩu' })
  password: string;
}
